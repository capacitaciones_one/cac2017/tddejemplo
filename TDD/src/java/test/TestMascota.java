package test;

import entidades.Mascota;

public class TestMascota {
    public static void main(String[] args) {
        
        //Caso de mascota creada con valores correctos
        Mascota miMascota = new Mascota("Cuco", 12, 'M');
        System.out.println(miMascota);
        
        //Caso de mascota creada con edad negativa
        Mascota miMascota1 = new Mascota("Chucky", 5, 'M');
        System.out.println(miMascota1);
        
        //Metodo pedir comida OK
        miMascota.pedirComida();
        miMascota1.pedirComida();
                
        
        
        System.out.println("[OK]");
    }
    
}
