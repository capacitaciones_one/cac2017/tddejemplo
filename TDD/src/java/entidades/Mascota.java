package entidades;

public class Mascota {
    private String nombre;
    private int edad;
    private char sexo;
    
    public Mascota(String nombre, int edad, char sexo) {
        setNombre(nombre);
        setEdad(edad);
        setSexo(sexo);
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        if (edad <= 0) {
            System.out.println("El animal " + this.nombre + " no puede tener edad negativa o cero. Animal!!!");
        } else {
            this.edad = edad;
        }
        
    }

    public void pedirComida() {
        System.out.println("Pido comida, soy :" + this.getNombre());
    }

    @Override
    public String toString() {
        return "Mascota{" + "nombre=" + nombre + ", edad=" + edad + ", sexo=" + sexo + '}';
    }

    
     
}
